import os
import os.path
from google.cloud import storage
import pandas as pd
from sharedVars import ricList
import numpy as np
from utils import datesMap, keysDates, mapDates, removeLateralZeros
from utils import getBetwDates, saveObj, rollingWindow
import dask.dataframe as dd
from sklearn.preprocessing import StandardScaler

class DatawarehouseService:
    __instance = None
    __envVarsService = None
    __daskClient = None
    ddf = None
    df = None
    ric = None

    def __init__(self, envVarsService, daskClient):
        self.__envVarsService = envVarsService
        self.__daskClient = daskClient;
    
    @staticmethod
    def getInstance(envVarsService, daskClient):
        if DatawarehouseService.__instance == None:
            DatawarehouseService.__instance = DatawarehouseService(envVarsService, daskClient)
        return DatawarehouseService.__instance

    def loadParquet(self):
        self.ddf = dd.read_parquet(self.__envVarsService.path_parquet_file, 
            engine='pyarrow',
            index = 'Date_Time',
            meta = pd.DataFrame(columns=ricList))
        self.ddf.index = self.ddf.index.astype('datetime64[ns, UTC]')

    def loadRic(self, ric):
        self.ric = ric
        self.df = self.ddf[[self.ric]].dropna().compute()
        self.df = self.df[self.df.index.dayofweek < 5]
        self.df = self.df.sort_index(axis = 0)
        
    def filterDF(self, date = '2020-01-12'):
        self.df = self.df[date > self.df.index]

    def preprocessRic1(self):
        self.df['date'] = pd.to_datetime(self.df.index).date.astype(str)
        self.df['log_diff'] = np.log(self.df[self.ric].astype('float64'))
        self.df['log_diff'] = self.df.groupby("date")['log_diff'].diff()
        self.df = self.df.fillna(0)
        self.df['aux'] = 0
        self.df = self.df.groupby("date").apply(removeLateralZeros)
        mask = self.df['aux'] > 0
        self.df = self.df.loc[mask]
        del self.df['aux']
        
    def preprocessRic2(self, df):
        #df['index'] = 1
        #df['index'] = df.groupby("date")['index'].cumsum()
        #df['index'] = df['index'] - 1
        #df = mapDates(df)
        df['timestamp'] = df.index
        df = df.reset_index(drop=True)
        df.columns = [[c,'y'][c == self.ric] for c in df.columns]
        df = df[['timestamp']+[c for c in df.columns][:-1]]
        df['LSTM_log_diff_scaled'] = 0
        df['GRU_log_diff_scaled'] = 0
        df['TRANSFORMER_log_diff_scaled'] = 0
        return df

    def genDatasets(self, limitDays = None):
        dates = self.df['date'].unique()
        if(limitDays != None):
            dates = dates[:limitDays]
        dates = np.array(np.array_split(dates, 7))
        dates = rollingWindow(dates,4,1)
        config = {'ric': self.ric}
        li = list(enumerate(dates))
        for i,d in li:#######################################################################
            folderExp =  os.path.join(self.__envVarsService.path_process_data, 'ric_'+self.ric, 'experimento_'+str(i))
            os.makedirs(folderExp, exist_ok=True)
            print('---------------')
            name = str(i)+'_'+d[0][0]+'_'+d[-1][-1]
            config[name] = {
                'train_start_date': d[0][0], 
                'train_end_date': d[-2][-1], 
                'test_start_date': d[-1][0], 
                'test_end_date': d[-1][-1]
            }
            df = getBetwDates(self.df, 
                              config[name]['train_start_date'], 
                              config[name]['test_end_date'])
            fileName = name+'.csv'

            df_train = getBetwDates(df, 
                              config[name]['train_start_date'], 
                              config[name]['train_end_date'])
            
            sc = StandardScaler()
            sc.fit_transform(df_train[['log_diff']])
            del df_train

            df['log_diff_scaled'] = sc.transform(df[['log_diff']])
            saveObj(folderExp+'/scaler.pickle', sc)

            self.preprocessRic2(df).to_csv(folderExp+'/dataset.csv',index=False)
          
            saveObj(folderExp+'/config.pickle', config[name])

        
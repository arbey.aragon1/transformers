import os
import os.path
from google.cloud import storage

class StorageService:
    __instance = None
    __envVarsService = None

    def __init__(self, envVarsService):
      self.__envVarsService = envVarsService
      client = storage.Client(project='mrl-nasdaq')
      self.bucket_name = 'mrl-nasdaq'
    
    @staticmethod
    def getInstance(envVarsService):
        if StorageService.__instance == None:
            StorageService.__instance = StorageService(envVarsService)
        return StorageService.__instance

    def downloadBlob(self, bucket_name, source_blob_name, destination_file_name):
        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(source_blob_name)
        blob.download_to_filename(destination_file_name)
        print(
            "Blob {} downloaded to {}.".format(source_blob_name, destination_file_name)
        )

    def loadDatabase(self):
        file_path = self.__envVarsService.path_download+'/data.zip'
        self.downloadBlob(self.bucket_name, 'parquet/data.zip', file_path)
        comman = 'unzip -d '+self.__envVarsService.path_raw_data+'/ '+file_path
        os.system(comman)

    
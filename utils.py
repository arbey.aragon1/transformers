import numpy as np
import pandas as pd
import pickle
import random
from sharedVars import datesMap, keysDates

def getSin(v, k):
    idx = v + datesMap[k]['offset']
    return datesMap[k]['sin'][idx]

def getCos(v, k):
    idx = v + datesMap[k]['offset']
    return datesMap[k]['cos'][idx]

def getSinCos(v, k):
    idx = v + datesMap[k]['offset']
    return datesMap[k]['sin'][idx], datesMap[k]['cos'][idx]

def getValueBySinCos(s, c, k):
    vec = (s - datesMap[k]['sin'])**2 + (c - datesMap[k]['cos'])**2
    idx = np.argmin(vec)
    return datesMap[k]['val'][idx]

def getColDate(df, k):
    if(k == 'month'):
        return df.index.month
    elif(k == 'dayofweek'):
        return df.index.dayofweek
    elif(k == 'day'):
        return df.index.day
    elif(k == 'hour'):
        return df.index.hour
    elif(k == 'minute'):
        return df.index.minute
    elif(k == 'second'):
        return df.index.second
    raise ValueError(k, 'key not defined.')

def mapDates(dfTest):
    for i,k in enumerate(keysDates):
        print('--'+str(i)+'/'+str(len(keysDates))+'--'+str(k))
        dfTest['sin_'+k] = getColDate(dfTest, k).map(lambda s:getSin(s, k))
        dfTest['cos_'+k] = getColDate(dfTest, k).map(lambda s:getCos(s, k))
    return dfTest

def flip_from_probability(v):
    return True if random.random() < v else False

def EMA(values, alpha=0.1):
    ema_values = [values[0]]
    for idx, item in enumerate(values[1:]):
        ema_values.append(alpha*item + (1-alpha)*ema_values[idx])
    return ema_values

def removeLateralZeros(df_day):
    df_day['aux'] = 1
    df_day['aux'] = df_day['aux'].cumsum()
    middle = df_day.shape[0]//2
    mask2 = df_day['aux'] >= middle
    mask1 = df_day['aux'] < middle
    df_day['aux'] = df_day['log_diff']
    df_day.loc[mask1, 'aux'] = df_day.loc[mask1, 'aux'].abs().cumsum()
    df_day.loc[mask2, 'aux'] = df_day.loc[mask2, 'aux'].loc[::-1].abs().cumsum().loc[::-1]
    #mask = df_day['aux'] > 0
    #del df_day['aux']
    return df_day#.loc[mask]

def rollingWindow(array, window_size, freq):
    shape = (array.shape[0] - window_size + 1, window_size)
    strides = (array.strides[0],) + array.strides
    rolled = np.lib.stride_tricks.as_strided(array, shape=shape, strides=strides)
    return rolled[np.arange(0,shape[0],freq)]

def saveObj(path, obj):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()

def loadObj(path):
    f = open(path, 'rb')
    obj = pickle.load(f)
    f.close()
    return obj

def getBetwDates(df, start_date, end_date):
    mask = (df['date'] >= start_date) & (df['date'] <= end_date)
    return df[mask]

def getDate(df, date):
    mask = (df['date'] == date)
    return df[mask]

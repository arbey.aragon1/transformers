import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from joblib import dump
import torch
from torch.utils.data import Dataset

class DataDataset(Dataset):
    def __init__(self, df, training_length, forecast_window):
        self.df = df
        self.transform = StandardScaler()
        self.T = training_length
        self.S = forecast_window
        cols = list(df.columns)
        self.cols = cols[cols.index('log_diff'):]

    def __len__(self):
        return 10

    # Will pull an index between 0 and __len__. 
    def __getitem__(self, idx):
        # np.random.seed(0)

        start = np.random.randint(0, len(self.df) - self.T - self.S) 

        index_in = torch.tensor([i for i in range(start, start+self.T)])
        index_tar = torch.tensor([i for i in range(start + self.T, start + self.T + self.S)])

        _input = torch.tensor(self.df[self.cols][start : start + self.T].values)
        target = torch.tensor(self.df[self.cols][start + self.T : start + self.T + self.S].values)

        ## scalar is fit only to the input, to avoid the scaled values "leaking" information about the target range.
        ## scalar is fit only for humidity, as the timestamps are already scaled
        ## scalar input/output of shape: [n_samples, n_features].
        scaler = self.transform
        scaler.fit(_input[:,0].unsqueeze(-1))

        _input[:,0] = torch.tensor(scaler.transform(_input[:,0].unsqueeze(-1)).squeeze(-1))
        target[:,0] = torch.tensor(scaler.transform(target[:,0].unsqueeze(-1)).squeeze(-1))

        dump(scaler, '/content/scalar_item.joblib')

        return index_in, index_tar, _input, target
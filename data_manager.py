from utils import flip_from_probability, EMA, rollingWindow
from utils import saveObj, loadObj, getBetwDates, getDate
import random
import numpy as np
import pandas as pd

class DataManager:
    def __init__(self,
                 df,
                 ric,
                 conf,
                 window,
                 forecast,
                 algorithm = 'LSTM'):
        self.variable = 'log_diff_scaled'
        self.algorithm = algorithm
        self.df = df
        self.col_prediction = self.algorithm + '_' + self.variable
        self.df[self.col_prediction] = 0
        self.ric = ric
        self.conf = conf
        self.window = window
        self.forecast = forecast
        self.padding = window + forecast
        self.genTrainTest(df)

    def genTrainTest(self, df):
        self.df_train = getBetwDates(df, 
                                self.conf['train_start_date'], 
                                self.conf['train_end_date'])
        self.dates_train = self.df_train['date'].unique()
        self.df_test = getBetwDates(df, 
                                self.conf['test_start_date'], 
                                self.conf['test_end_date'])
        self.dates_test = self.df_test['date'].unique()

    def getSampleDayTrainKey(self):
        return random.choice(self.dates_train)

    def getSampleDayTestKey(self):
        return random.choice(self.dates_test)

    def getDay(self, date):
        return getDate(self.df, date)

    def getSampleIndex(self, totalLen):
        idx_max = totalLen - self.padding
        idx = random.randint(0,idx_max)
        return idx

    def getSampleDataFromDay(self, date):
        df_day = self.getDay(date)
        idx = self.getSampleIndex(df_day.shape[0])
        window = df_day[idx: idx + self.window]
        forecast = df_day[idx + self.window: idx + self.window + self.forecast]
        return window, forecast

    def getSampleDataTrain(self):
        lenData = self.df_train.shape[0]
        while True:
            idx = self.getSampleIndex(lenData)
            window = self.df_train[idx: idx + self.window]
            forecast = self.df_train[idx + self.window: idx + self.window + self.forecast]
            yield window, forecast

    def getSampleDataTest(self):
        lenData = self.df_test.shape[0]
        while True:
            idx = self.getSampleIndex(lenData)
            window = self.df_test[idx: idx + self.window]
            forecast = self.df_test[idx + self.window: idx + self.window + self.forecast]
            yield window, forecast

    def getSampleDayIter(self, date):
        df_day = self.getDay(date)
        totalLen = df_day.shape[0]
        idx_max = totalLen - self.padding
        while True:
            for idx in range(0, idx_max+1):
                window = df_day[idx: idx + self.window]
                forecast = df_day[idx + self.window: idx + self.window + self.forecast]
                yield window, forecast
    
    def setPrediction(self, target, new_value):
        self.df.loc[target.index, self.col_prediction] = new_value

    def saveData(self):
        fileName = self.k + '.csv'
        #columns = ['log_diff_scaled', self.col_prediction]
        #[['timestamp', self.ric, 'date', 'log_diff']+columns]
        self.df.to_csv('datasets/'+fileName,index=False)
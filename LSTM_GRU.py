#!/usr/bin/env python
# coding: utf-8

# In[3]:


from dask.diagnostics import ProgressBar 
from dask.distributed import Client
from google.cloud import bigquery
from google.cloud import storage
from google.cloud.bigquery.client import Client
from icecream import ic
from icecream import ic 
from itertools import combinations
from joblib import dump
from joblib import load
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler
import argparse
import dask
import dask.dataframe as dd
import logging
import math
import math, random
import matplotlib.pyplot as plt
import memory_profiler
import multiprocessing as mp
import numpy as np
import os
import os, shutil
import pandas as pd
import random
import time
import warnings
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential 
from keras.layers import Dense, Activation, LSTM, GRU, SimpleRNN
from keras.preprocessing.sequence import TimeseriesGenerator
import random
import pickle
from sklearn.preprocessing import StandardScaler
import sys
from keras.utils.vis_utils import plot_model
from IPython.display import Image
import time
import subprocess
import logging
logging.basicConfig(filename='log.log', level=logging.DEBUG, filemode='w')

warnings.filterwarnings('ignore')
sys.path.insert(1, './transformers')

print("Pandas version: ", pd.__version__)
print("Dask   version: ", dask.__version__)


# In[4]:


from utils import flip_from_probability, EMA, rollingWindow
from utils import saveObj, loadObj, getBetwDates, getDate
from data_manager import DataManager
from module import Module


# In[ ]:





# In[5]:


daskClient = Client()#n_workers=8
module = Module.getInstance(daskClient)
envVarsService = module.EnvVarsService()
storageService = module.StorageService()
datawarehouseService = module.DatawarehouseService()


# In[ ]:





# In[ ]:





# In[ ]:





# In[6]:


window = 60
forecast = 1
nb_epoch = 300#10000
batch_size = 3


# In[ ]:





# In[8]:


ricList = subprocess.check_output(['ls', 'process-data']).decode('utf-8').split('\n')
ricList = [r.replace('ric_','') for r in ricList if len(r) and ('AAPL.OQ' not in r)]
ricList = ['AAPL.OQ']+ricList
ricList


# In[ ]:


from keras.layers.core import Dense, Dropout


# In[39]:


def trainProcess(ric, exp, modelType = 'LSTM', epoch = 1):
    #Carpeta de ric
    folderRic = 'ric_'+ric

    #Ruta del repo
    path_main = os.getcwd()

    #Ruta del experimento
    expPath = path_main+'/process-data/'+folderRic+'/'+exp

    #Configuracion del experimento
    config = loadObj(expPath+'/config.pickle')

    #Datos
    filePath = expPath+'/dataset.csv'
    df = pd.read_csv(filePath)

    #Administrador de datos
    dataManager = DataManager(df,
                     ric,
                     config,
                     window,
                     forecast,
                     algorithm = 'Transformer')

    #Generadores
    train_generator = TimeseriesGenerator(
        np.reshape(dataManager.df_train['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        np.reshape(dataManager.df_train['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        length=window, 
        batch_size=512)     
    test_generator = TimeseriesGenerator(
        np.reshape(dataManager.df_test['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        np.reshape(dataManager.df_test['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        length=window, 
        batch_size=512)

    #Cantidad de pasos
    steps_per_epoch = (dataManager.df_train.shape[0])/batch_size
    validation_steps = (dataManager.df_test.shape[0])/batch_size

    dim_entrada = (window, 1)
    dim_salida = forecast

    if(modelType == 'LSTM'):
        #Modelo
        model = Sequential()
        model.add(LSTM(units=50, input_shape=dim_entrada))
        model.add(Dense(units=dim_salida))
        model.add(Dropout(0.2))
        model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
    if(modelType == 'GRU'):
        #Modelo
        model = Sequential()
        #model.add(GRU(window, input_shape=dim_entrada))
        #model.add(SimpleRNN(dim_salida))
        
        model.add(GRU(50, input_shape=dim_entrada))
        model.add(Dense(units=dim_salida))
        model.add(Dropout(0.2))
        
        model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])

    #Entrenamiento
    history = model.fit_generator(
        train_generator,
        epochs = epoch,
        verbose=1,
        validation_data=test_generator).history

    if(epoch != 1):
        #Salva modelos
        model.save(expPath+'/'+modelType+'_model.h5', overwrite=True)
        saveObj(expPath+'/'+modelType+'_model.history', history)


# In[1]:


#list de experimentos
expList = ['experimento_'+str(i) for i in range(4)]


# In[1]:


logging.info('Start')
params = []
counter = 0
for r in ricList:
    for exp in expList:
        for m in ['LSTM', 'GRU']:
            params.append([r,exp,m, counter])
            counter = counter + 1


# In[ ]:


def strainStep(r, exp, m):
    start = time.time()
    logging.debug(r)
    logging.debug(exp)
    logging.debug(m)
    trainProcess(r, exp, m)
    end = time.time()
    seconds = (end - start)
    logging.debug("--- %s Seconds port iter ---" % seconds)
    hours = seconds * float(1/60) * float(1/60) * nb_epoch 
    logging.debug("--- %s Hours ---" % hours)
    hoursAllRics =  hours * len(ricList) 
    logging.debug("--- %s Hours al rics ---" % hoursAllRics)
    daysAllRics =  hoursAllRics * float(1/24)
    logging.debug("--- %s Days al rics ---" % daysAllRics)
    return daysAllRics


# In[ ]:


daysAllRics = 0
r, exp, m, c = params[0]
daysAllRics += strainStep(r, exp, m)

r, exp, m, c = params[1]
daysAllRics += strainStep(r, exp, m)

logging.debug("--- %s Days al rics LSTM GRU ---" % daysAllRics)


# In[ ]:


for r, exp, m, c in params[4:]:
    logging.debug('------------------------------')
    start = time.time()
    logging.debug(str(c)+"/"+str(counter))
    logging.debug(r)
    logging.debug(exp)
    logging.debug(m)
    trainProcess(r, exp, m, epoch = nb_epoch)
    end = time.time()
    logging.debug(end - start)


# In[ ]:





# In[ ]:





# In[22]:





# In[ ]:





# In[24]:


#plot_model(model, to_file='model.png', show_shapes=True)
#Image(filename='model.png')


# In[27]:


#model.evaluate_generator(test_data_gen)
#trainPredict = model.predict_generator(train_data_gen)
#testPredict = model.predict_generator(test_data_gen)


# In[ ]:





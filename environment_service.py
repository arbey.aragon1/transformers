import os
import os.path
import shutil

class EnvVarsService:
    __instance = None

    def __init__(self, path_main = None):
        if(path_main == None):
            path_main = os.getcwd()
            print("----------------------")
            print(path_main)

        self.path_main = path_main
        self.path_data = self.path_main+'/data'
        self.path_download = self.path_main+'/download'
        self.path_raw_data = self.path_main+'/raw-data'
        self.path_parquet_file = self.path_raw_data+'/home/jupyter/data4/data.parquet'
        self.path_process_data = self.path_main+'/process-data'
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = self.path_main+'/credentials/cred.json'
        print(self.path_main+'/credentials/cred.json')
    
    @staticmethod
    def getInstance():
        if EnvVarsService.__instance == None:
            EnvVarsService.__instance = EnvVarsService()
        return EnvVarsService.__instance

    def mkdir(self, path):
        try:
            os.mkdir(path)
        except OSError as error:
            print(error) 

    def clean_directory(self):
        if os.path.exists(self.path_data):
            shutil.rmtree(self.path_data)
        if os.path.exists(self.path_download): 
            shutil.rmtree(self.path_download)
        if os.path.exists(self.path_raw_data): 
            shutil.rmtree(self.path_raw_data)

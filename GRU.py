#!/usr/bin/env python
# coding: utf-8

# In[41]:


from dask.diagnostics import ProgressBar 
from dask.distributed import Client
from google.cloud import bigquery
from google.cloud import storage
from google.cloud.bigquery.client import Client
from icecream import ic
from icecream import ic 
from itertools import combinations
from joblib import dump
from joblib import load
from matplotlib import pyplot
from sklearn.preprocessing import MinMaxScaler
import argparse
import dask
import dask.dataframe as dd
import logging
import math
import math, random
import matplotlib.pyplot as plt
import memory_profiler
import multiprocessing as mp
import numpy as np
import os
import os, shutil
import pandas as pd
import random
import time
import warnings
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential 
from keras.layers import Dense, Activation, LSTM, GRU, SimpleRNN
from keras.preprocessing.sequence import TimeseriesGenerator
import random
import pickle
from sklearn.preprocessing import StandardScaler
import sys
from keras.utils.vis_utils import plot_model
from IPython.display import Image
import time
import subprocess
#%load_ext memory_profiler
#%matplotlib inline

warnings.filterwarnings('ignore')
sys.path.insert(1, './transformers')

print("Pandas version: ", pd.__version__)
print("Dask   version: ", dask.__version__)


# In[2]:


from utils import flip_from_probability, EMA, rollingWindow
from utils import saveObj, loadObj, getBetwDates, getDate
from data_manager import DataManager
from module import Module


# In[3]:





# In[4]:


daskClient = Client()#n_workers=8
module = Module.getInstance(daskClient)
envVarsService = module.EnvVarsService()
storageService = module.StorageService()
datawarehouseService = module.DatawarehouseService()


# In[5]:





# In[6]:





# In[ ]:





# In[36]:


window = 60
forecast = 1
nb_epoch = 10#10000
batch_size = 3


# In[ ]:





# In[7]:


ricList = subprocess.check_output(['ls', 'process-data']).decode('utf-8').split('\n')
ricList = [r.replace('ric_','') for r in ricList if len(r)][:10]


# In[ ]:





# In[39]:


def trainProcess(ric):
    #Carpeta de ric
    folderRic = 'ric_'+ric

    #list de experimentos
    expList = ['experimento_'+str(i) for i in range(4)]

    #Ruta del repo
    path_main = os.getcwd()

    #Ruta del experimento
    expPath = path_main+'/process-data/'+folderRic+'/'+expList[0]

    #Configuracion del experimento
    config = loadObj(expPath+'/config.pickle')

    #Datos
    filePath = expPath+'/dataset.csv'
    df = pd.read_csv(filePath)

    #Administrador de datos
    dataManager = DataManager(df,
                     ric,
                     config,
                     window,
                     forecast,
                     algorithm = 'Transformer')

    #Generadores
    train_generator = TimeseriesGenerator(
        np.reshape(dataManager.df_train['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        np.reshape(dataManager.df_train['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        length=window, 
        batch_size=20)     
    test_generator = TimeseriesGenerator(
        np.reshape(dataManager.df_test['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        np.reshape(dataManager.df_test['log_diff_scaled'].values.astype('float32'),(-1,1)), 
        length=window, 
        batch_size=1)

    #Cantidad de pasos
    steps_per_epoch = (dataManager.df_train.shape[0])/batch_size
    validation_steps = (dataManager.df_test.shape[0])/batch_size

    dim_entrada = (window, 1)
    dim_salida = forecast

    #Modelo
    model = Sequential()
    model.add(GRU(window, return_sequences=True))
    model.add(SimpleRNN(dim_salida))
    model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])

    #Entrenamiento
    history = model.fit_generator(
        train_generator,
        epochs = nb_epoch,
        verbose=1,
        validation_data=test_generator).history

    #Salva modelos
    model.save_weights(expPath+'/GRU_model.h5', overwrite=True)
    saveObj(expPath+'/GRU_model.history', history)
    
    
    


# In[ ]:





# In[42]:


start = time.time()
print(ricList)
for r in ricList:
    print(r)
    trainProcess(r)
end = time.time()
print(end - start)


# In[ ]:





# In[ ]:





# In[22]:





# In[ ]:





# In[24]:


#plot_model(model, to_file='model.png', show_shapes=True)
#Image(filename='model.png')


# In[27]:


#model.evaluate_generator(test_data_gen)
#trainPredict = model.predict_generator(train_data_gen)
#testPredict = model.predict_generator(test_data_gen)


# In[ ]:





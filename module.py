from environment_service import EnvVarsService
from storage_service import StorageService
from datawarehouse_service import DatawarehouseService

class Module():
    __instance = None
    __envVarsService = None
    __storageService = None
    __datawarehouseService = None

    def __init__(self, daskClient):
        self.__loadModules(daskClient)

    @staticmethod
    def getInstance(daskClient):
        if Module.__instance == None:
            Module.__instance = Module(daskClient)
        return Module.__instance

    def __loadModules(self, daskClient):
        self.__envVarsService = EnvVarsService.getInstance()
        self.__storageService = StorageService.getInstance(self.__envVarsService)
        self.__datawarehouseService = DatawarehouseService.getInstance(self.__envVarsService, daskClient)

        
    def EnvVarsService(self):
        return self.__envVarsService

    def StorageService(self):
        return self.__storageService

    def DatawarehouseService(self):
        return self.__datawarehouseService
    